# How to build the project on Windows.

Run from powershell:

* `.\build.ps1`

To run unit tests:

* `ctest --VV`