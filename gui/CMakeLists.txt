get_filename_component(CUSTOM_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
project(${CUSTOM_NAME})

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

set(SRC gui.cpp)
set(HEADERS shell.h)
set(EXECUTABLE_OUTPUT_PATH "${CMAKE_BINARY_DIR}/Out")

find_package(Qt5Widgets CONFIG REQUIRED)
find_package(Qt5Gui CONFIG REQUIRED)
find_package(Qt5Core CONFIG REQUIRED)
add_executable(${PROJECT_NAME} WIN32 ${HEADERS} ${SRC})
target_link_libraries(${PROJECT_NAME} PRIVATE
	Qt5::Core
	Qt5::WinMain
	Qt5::CorePrivate
	Qt5::Gui
	Qt5::GuiPrivate
	Qt5::Widgets
	Qt5::WidgetsPrivate)