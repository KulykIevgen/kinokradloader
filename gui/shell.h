#pragma once
#include <QProcess>
#include <QtWidgets/QtWidgets>


class MyShell: public QWidget
{
	Q_OBJECT
public:
	MyShell(QTextEdit* output) : QWidget(output),
		process(new QProcess()), m_output(output)
	{
		QObject::connect(process,
			SIGNAL(readyRead()),
			SLOT(PrintOutput()));
	}
	void start(const QString& params) const
	{
		process->start(params);
		m_output->append("Starting " + params);
		process->waitForReadyRead();
	}

private:
	QProcess* process;
	QTextEdit* m_output;

	Q_SLOT void PrintOutput()
    {
	    while (process->canReadLine())
	    {
		    m_output->append(process->readLine());
	    }
    }
};