#include "shell.h"


int main(int argc, char* argv[])
{
	QApplication application(argc, argv);
	QFrame wgt;
	QVBoxLayout* layout = new QVBoxLayout(&wgt);
	QLabel* urlLabel = new QLabel("Input film url here:");
	QLabel* statusLabel = new QLabel("Read the output of downloading process");
	QLineEdit* urlInput = new QLineEdit;
	QTextEdit* statusOutput = new QTextEdit;
	statusOutput->setReadOnly(true);
	QPushButton* start = new QPushButton("Start");
	const auto currentDir = application.applicationDirPath();

	auto StartButtonHandler = [urlInput, statusOutput, &currentDir]() -> void
	{
		const auto filmUrl = urlInput->text();
		MyShell* filmLoader = new MyShell(statusOutput);
		filmLoader->start(currentDir + "\\kinokradLoader.exe " + filmUrl);
	};

	QObject::connect(start, &QPushButton::clicked, StartButtonHandler);

	layout->addWidget(urlLabel);
	layout->addWidget(urlInput);
	layout->addWidget(statusLabel);
	layout->addWidget(statusOutput);
	layout->addWidget(start);

	wgt.setFrameStyle(QFrame::Box | QFrame::Plain);
	wgt.setFixedSize(500, 400);
	wgt.show();

	return application.exec();
}