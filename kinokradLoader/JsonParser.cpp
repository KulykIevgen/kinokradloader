#include "JsonParser.h"

JsonParser::JsonParser(const std::string& data)
{
	std::string currentValue;
	size_t position = 0;
	bool foundInRound = false;

	while (position < data.length())
	{
		for (const auto& terminal : terminals)
		{
			const size_t len = terminal.length();
			const auto substring = data.substr(position, len);
			if (substring == terminal)
			{
				foundInRound = true;
				position += len;
				if (currentValue.length())
				{
					tokens.push_back(currentValue);
					currentValue = {};
				}
				break;
			}
		}

		if (!foundInRound)
		{
			currentValue += data[position++];
		}
		foundInRound = false;
	}
}

std::vector<std::string> JsonParser::findAllTokensAfterValue(const std::string& value) const
{
	std::vector<std::string> result;

	for (auto it = tokens.cbegin(); it != tokens.cend(); ++it)
	{
		if (*it == value)
		{
			result.push_back(*std::next(it));
		}
	}

	return result;
}

std::vector<std::string> JsonParser::sanytizeUrls(const std::vector<std::string>& urls) const
{
	std::vector<std::string> result;

	for (const auto& url : urls)
	{
		std::string sanytized;

		for (const auto& symbol : url)
		{
			if (symbol != '\\')
			{
				sanytized += symbol;
			}
		}

		result.push_back(sanytized);
	}

	return result;
}
