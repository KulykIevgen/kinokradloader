#pragma once

#include <list>
#include <iostream>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>
#include <Windows.h>
#include <WinInet.h>
#pragma comment(lib,"Wininet.lib")

struct FilmSource
{
	std::string FilmFilesDirectory;
	std::string PathToPlaylist;
};

enum class ContentType
{
	film,
	series
};

class InetApi
{
public:
	InetApi();
	virtual ~InetApi() noexcept;
	std::string GetPageContent(const std::wstring& url);
	FilmSource GetFilmSource(const std::string& pageContent);	
	std::pair<std::list<std::string>, unsigned int>
		CreateDownloadPartsFromFilmSource(const FilmSource& source);
	void DownloadFilesToDirectory(const std::list<std::string>& urlsForFiles,
		const std::wstring& directory, unsigned int count);
	std::wstring CreateDirectoryFromUrl(const std::wstring& url);
	void BuildMovieFromChunks(const std::wstring& directory, 
		const std::wstring& builder, const std::wstring& filmName);
	ContentType GetContentType(const std::wstring& url);
	std::vector<std::string> GetSeriesUrls(const std::wstring& url);
	std::wstring GetTopDir(const std::wstring& url) const;
	std::wstring GetTopName(const std::wstring& url) const;

private:
	void DownloadFileToDirectory(const std::string& url, const std::wstring& directory);
	std::string GetSeriesSubdir(const std::string& pageContent) const;	

	template <typename T>
	T GetShortName(const T& path);
	std::wstring GetCurrentDirectoryPath() const;
	std::tuple<std::wstring, std::vector<std::wstring>> CreateTsMetaFile(const std::wstring& workingDirectory) const;
	HINTERNET m_connection;
	std::wostream& logStream = std::wcout;
};

