#include "InetApi.h"
#include "JsonParser.h"
#include "ErrorInfo.h"
#include "string_utils.h"

#include <algorithm>
#include <atomic>
#include <chrono>
#include <execution>
#include <filesystem>
#include <fstream>
#include <memory>
#include <regex>
#include <stdexcept>
#include <sstream>
#include <thread>
#include <type_traits>
#include <urlmon.h>
#pragma comment(lib, "Urlmon.lib")

InetApi::InetApi()
{
	m_connection = InternetOpenW(L"Microsoft Internet Explorer",
		INTERNET_OPEN_TYPE_PRECONFIG, nullptr, nullptr, 0);
	if (!m_connection)
	{
		throw ErrorInfo::MakeLastError();
	}
}

InetApi::~InetApi() noexcept
{
	InternetCloseHandle(m_connection);
}

std::string InetApi::GetPageContent(const std::wstring& url)
{
	logStream << L"Getting raw content from url: " << url << std::endl;
	std::string result;
	auto urlObject = InternetOpenUrlW(m_connection, url.c_str(),
		nullptr, 0, 0, 0);

	if (!urlObject)
	{
		const auto message = std::wstring(L"Unable to open ") + url;
		const auto asci_message = StringUtils::StringFromUtf8(message);
		throw std::exception(asci_message.c_str());
	}

	constexpr unsigned int dataSize = 1000000;
	std::unique_ptr<char> data(new char[dataSize]);
	memset(data.get(), 0, dataSize);

	INTERNET_BUFFERSA buffers{};
	buffers.dwStructSize = sizeof(buffers);
	unsigned int offset = 0;
	bool readResult;

	do
	{
		buffers.dwBufferLength = dataSize - offset;
		buffers.lpvBuffer = data.get() + offset;
		readResult = InternetReadFileExA(urlObject, &buffers, IRF_SYNC, 0);

		if (readResult && buffers.dwBufferLength)
		{
			if (offset + buffers.dwBufferLength > dataSize)
			{
				break;
			}
			offset += buffers.dwBufferLength;
		}
		else
		{
			break;
		}

	} while (readResult);

	if (offset)
	{
		result = std::string(data.get(), offset);
	}
	else
	{
		result = "Error: ";
		result += std::to_string(GetLastError());
	}

	InternetCloseHandle(urlObject);
	logStream << L"Ok, content recieved\n\n" << std::endl;
	return result;
}

FilmSource InetApi::GetFilmSource(const std::string& pageContent)
{
	logStream << L"Search film playlist.m3u8 by regex" << std::endl;
	std::string playlist = "/playlist.m3u8";
	std::regex re("var filmSource = \"(.*)/playlist.m3u8");
	std::smatch match;
	std::string film;

	if (std::regex_search(pageContent, match, re) && match.size() > 1)
	{
		film = match.str(1);
	}
	logStream << L"Url found: " << StringUtils::Utf8FromString(film) << L"\n\n" << std::endl;
	return FilmSource{ film, film + playlist};
}

std::string InetApi::GetSeriesSubdir(const std::string& pageContent) const
{
	std::regex re("/playlist/([[:digit:]]+).txt");
	std::smatch match;
	std::string subdir;

	if (std::regex_search(pageContent, match, re) && match.size() > 1)
	{
		subdir = match.str(0);
	}
	return subdir;
}

std::wstring InetApi::GetTopDir(const std::wstring& url) const
{
	const auto found = url.find_last_of(L'/');
	if (found == std::wstring::npos)
	{
		return url;
	}
	return url.substr(0, found);
}

std::wstring InetApi::GetTopName(const std::wstring& url) const
{
	const auto found = url.find_last_of(L'/');
	if (found == std::wstring::npos)
	{
		return url;
	}
	return url.substr(found + 1);
}

std::pair<std::list<std::string>, unsigned int>
InetApi::CreateDownloadPartsFromFilmSource(const FilmSource& source)
{
	logStream << L"Searching for the parts" << std::endl;
	std::list<std::string> result;
	unsigned int count = 0;

	const auto& pathToPlaylist = source.PathToPlaylist;
	const auto wideCharPlaylistPath = StringUtils::Utf8FromString(pathToPlaylist);
	const auto playlistContent = this->GetPageContent(wideCharPlaylistPath);
	std::istringstream playlistStream(playlistContent);

	std::string playlistPart;
	while (std::getline(playlistStream, playlistPart))
	{
		if (playlistPart.length() && playlistPart[0] != '#')
		{
			++count;
			result.push_back(source.FilmFilesDirectory + std::string("/") + playlistPart);
		}
	}

	logStream << L"Found " << count << L" parts\n\n" << std::endl;
	return std::make_pair(result, count);
}

void InetApi::DownloadFilesToDirectory(const std::list<std::string>& urlsForFiles,
	const std::wstring& directory, unsigned int count)
{
	logStream << L"Starting downloading chunks" << std::endl;
	std::atomic<unsigned int> downloadedChunks{ 0 };
	std::atomic<unsigned int> failedToDownload{ 0 };
	std::atomic_bool finished(false);

	std::thread progress([&]()
	{
		auto redraw = [](const std::atomic<unsigned int>& success,
			const std::atomic<unsigned int>& failed, const unsigned int total)
		{
			const auto percents = (success.load() + failed.load()) * 100 / total;
			std::cout << "Downloading... " << percents << " %" << std::endl;
		};

		while (true)
		{
			using namespace std::chrono_literals;
			std::this_thread::sleep_for(1s);
			redraw(downloadedChunks, failedToDownload, count);
			if (finished)
			{
				std::cout << std::endl;
				break;
			}
		}
	});

	std::for_each(std::execution::par_unseq, urlsForFiles.cbegin(), urlsForFiles.cend(),
		[&](auto url)
	{
		try {
			DownloadFileToDirectory(url, directory);
			++downloadedChunks;
		}
		catch (const std::exception& error)
		{
			std::cout << "Error " << error.what() << " during downloading " << url << std::endl;
			++failedToDownload;
		}
	});

	finished.store(true);
	progress.join();

	logStream << L"Finish downloading chunks\n\n" << std::endl;
}

std::wstring InetApi::CreateDirectoryFromUrl(const std::wstring& url)
{
	const auto directoryName = GetShortName(url);
	const auto currentDir = GetCurrentDirectoryPath();
	const auto fullDirPath = currentDir + L'\\' + directoryName;

	logStream << L"Trying to create directory from url: " << url << std::endl;
	if (!CreateDirectoryW(fullDirPath.c_str(), nullptr))
	{
		throw std::exception("Unable to create directory");
	}
	logStream << L"Ok, directory: " << fullDirPath << L" created\n\n" << std::endl;

	return fullDirPath;
}

void InetApi::BuildMovieFromChunks(const std::wstring& directory,
	const std::wstring& builder, const std::wstring& filmName)
{
	logStream << L"Crafting metafile" << std::endl;
	const auto [metafile, videoParts] = CreateTsMetaFile(directory);
	logStream << metafile << L" was created" << std::endl;

	const auto resultFile = directory + L"\\" + filmName + L".ts";
	const auto commandLine = L'"' + builder + L"\" " + metafile + L' ' + resultFile;

	STARTUPINFOW startup { 0 };
	PROCESS_INFORMATION processInfo { 0 };

	logStream << L"Launching external builder " << builder << std::endl;

	if (!CreateProcessW(nullptr, (LPWSTR)(commandLine.c_str()), nullptr, nullptr,
		FALSE, CREATE_NO_WINDOW, nullptr, nullptr, &startup, &processInfo))
	{
		throw ErrorInfo::MakeLastError();
	}

	logStream << L"Waiting for external builder " << builder << std::endl;
	WaitForSingleObject(processInfo.hProcess, INFINITE);

	logStream << L"Remove temp files" << std::endl;
	DeleteFileW(metafile.c_str());
	for (const auto& part : videoParts)
	{
		DeleteFileW(part.c_str());
	}
	logStream << L"Build finished, result: " << resultFile << std::endl;
}

ContentType InetApi::GetContentType(const std::wstring& url)
{
	const auto pageContent = GetPageContent(url);
	const auto seriesSubDir = GetSeriesSubdir(pageContent);
	if (!seriesSubDir.length())
	{
		return ContentType::film;
	}
	return ContentType::series;
}

std::vector<std::string> InetApi::GetSeriesUrls(const std::wstring& url)
{
	const auto pageContent = GetPageContent(url);
	const auto seriesSubDir = GetSeriesSubdir(pageContent);
	const auto seriesTopDir = GetTopDir(url);
	const auto seriesJson = StringUtils::StringFromUtf8(seriesTopDir) + seriesSubDir;
	const auto seriesJsonUrl = StringUtils::Utf8FromString(seriesJson);
	const auto seriesList = GetPageContent(seriesJsonUrl);

	JsonParser parser(seriesList);
	const auto urls = parser.findAllTokensAfterValue("file");
	return parser.sanytizeUrls(urls);
}

void InetApi::DownloadFileToDirectory(const std::string& url, const std::wstring& directory)
{
	const auto shortName = GetShortName(url);
	const auto wideCharName = StringUtils::Utf8FromString(shortName);
	const auto fileName = directory + std::wstring(L"\\") + wideCharName;
	const auto wideUrl = StringUtils::Utf8FromString(url);

	if (auto result = URLDownloadToFileW(nullptr, wideUrl.c_str(), fileName.c_str(), 0, nullptr);
		result != S_OK)
	{
		if (result == E_OUTOFMEMORY)
		{
			throw std::exception("Out of memory");
		}
		else if (result == INET_E_DOWNLOAD_FAILURE)
		{
			throw std::exception("Download failure");
		}
	}
}

std::wstring InetApi::GetCurrentDirectoryPath() const
{
	unsigned int size = 100;
	std::unique_ptr<wchar_t> currentDirBuffer(new wchar_t[size]);
	memset(currentDirBuffer.get(), 0, size);

	while (const auto preferedSize = ::GetCurrentDirectoryW(size, currentDirBuffer.get()))
	{
		if (preferedSize > size)
		{
			size = preferedSize;
			currentDirBuffer.reset(new wchar_t[size]);
			memset(currentDirBuffer.get(), 0, size);
		}
		else
		{
			return std::wstring(currentDirBuffer.get(), wcslen(currentDirBuffer.get()));
		}
	}

	return {};
}

std::tuple<std::wstring, std::vector<std::wstring>> InetApi::CreateTsMetaFile(const std::wstring& workingDirectory) const
{
	const auto metafile = workingDirectory + L"\\metafile.meta";
	std::filesystem::path dir(workingDirectory);

	std::vector<std::wstring> files;
	for (const auto& file : std::filesystem::directory_iterator(dir))
	{
		files.emplace_back(file.path().wstring());
	}
	std::sort(files.begin(), files.end());

	std::wofstream metastream(metafile);
	metastream << L"MUXOPT --no-pcr-on-video-pid --new-audio-pes --vbr  --vbv-len=500\n";
	metastream << L"V_MPEG4/ISO/AVC, ";
	bool isFirst = true;
	for (const auto& file : files)
	{
		if (!isFirst)
		{
			const auto element = L"+\"" + file + L'"';
			metastream << element;
		}
		else
		{
			const auto element = L'"' + file + L'"';
			metastream << element;
			isFirst = false;
		}
	}

	metastream << L", insertSEI, contSPS, track=256\n";
	metastream << L"A_AAC, ";
	isFirst = true;
	for (const auto& file : files)
	{
		if (!isFirst)
		{
			const auto element = L"+\"" + file + L'"';
			metastream << element;
		}
		else
		{
			const auto element = L'"' + file + L'"';
			metastream << element;
			isFirst = false;
		}
	}

	metastream << L", timeshift=-22ms, track=257";
	metastream.close();

	return std::make_tuple(metafile, files);
}

template <typename T>
T InetApi::GetShortName(const T& path)
{
	using symbol_type = typename T::value_type;
	constexpr size_t symbol_raw_size = 1;

	if constexpr (std::is_same<symbol_type, char>::value)
	{
		const auto separator = '/';
		const auto position = path.find_last_of(separator);
		if (position != T::npos)
		{
			return path.substr(position + symbol_raw_size);
		}
	}
	else if constexpr (std::is_same<symbol_type, wchar_t>::value)
	{
		const auto separator = L'/';
		const auto position = path.find_last_of(separator);
		if (position != T::npos)
		{
			return path.substr(position + symbol_raw_size);
		}
	}
	else
	{
		static_assert("unable to instantiate shortname with such type");
	}

	return {};
}
