#pragma once

#include <string>

namespace StringUtils
{
	/**
	 * \brief wstring to string conversion
	 * \param data is a link to wstring object
	 * \return string object
	 */
	std::string StringFromUtf8(const std::wstring& data);
	/**
	 * \brief string to wstring conversion
	 * \param data is a link to string object
	 * \return wstring object
	 */
	std::wstring Utf8FromString(const std::string& data);
}
