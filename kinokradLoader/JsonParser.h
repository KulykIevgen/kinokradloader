#pragma once

#include <string>
#include <vector>

class JsonParser
{
public:
	explicit JsonParser(const std::string& data);
	std::vector<std::string> findAllTokensAfterValue(const std::string& value) const;
	std::vector<std::string> sanytizeUrls(const std::vector<std::string>& urls) const;

private:
	std::vector<std::string> tokens;
	const std::vector<std::string> terminals{
		"{",
		"\":\"",
		"\"",
		"[",		
		",",
		"]",
		"}"
	};
};

