#include "ErrorInfo.h"

std::logic_error ErrorInfo::MakeErrorFromCode(DWORD ErrorCode)
{
	LPSTR messageBuffer = nullptr;

	const auto size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		nullptr, ErrorCode,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPSTR)&messageBuffer, 0, nullptr);
	const std::string errorMessage(messageBuffer, size);
	LocalFree(messageBuffer);

	return std::logic_error(errorMessage);
}

std::logic_error ErrorInfo::MakeLastError()
{
	return ErrorInfo::MakeErrorFromCode(GetLastError());
}