#include "string_utils.h"

#include <boost/locale.hpp>
#include <stdexcept>

std::string StringUtils::StringFromUtf8(const std::wstring& data)
{
	if (!data.length())
	{
		return {};
	}

	const auto result = boost::locale::conv::utf_to_utf<char>(data.c_str());
	if (result.length())
	{
		return result;
	}

	throw std::exception("Bad wstring cast");
}

std::wstring StringUtils::Utf8FromString(const std::string& data)
{
	if (!data.length())
	{
		return {};
	}

	const auto result = boost::locale::conv::utf_to_utf<wchar_t>(data.c_str());
	if (result.length())
	{
		return result;
	}

	throw std::exception("Bad string cast");
}
