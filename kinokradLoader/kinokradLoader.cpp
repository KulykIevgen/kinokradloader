﻿#include <filesystem>
#include <iostream>

#include "InetApi.h"
#include "string_utils.h"

int wmain(int argc, wchar_t* argv[]) try
{
	if (argc != 2)
	{
		std::cout << "Use: kinokradLoader.exe \"url_to_film\"" << std::endl;
	}
	else
	{
		InetApi inet;
		const auto working_url = std::wstring(argv[1]);

		const auto type = inet.GetContentType(working_url);
		switch (type)
		{
		case ContentType::film:
			{
				const auto working_directory = inet.CreateDirectoryFromUrl(working_url);
				const auto page_content = inet.GetPageContent(working_url);
				const auto filmData = inet.GetFilmSource(page_content);
				const auto film = StringUtils::Utf8FromString(filmData.FilmFilesDirectory);
				const auto filmName = inet.GetTopName(film);
				const auto [files, count] = inet.CreateDownloadPartsFromFilmSource(filmData);
				inet.DownloadFilesToDirectory(files, working_directory, count);
				inet.BuildMovieFromChunks(working_directory, L"tsMuxeR.exe", filmName);
			}
			break;
		case ContentType::series:
			{
				const auto list = inet.GetSeriesUrls(working_url);
				const auto working_directory = inet.CreateDirectoryFromUrl(working_url);
				std::filesystem::path workDir(working_directory);
				unsigned int i = 1;

				for (const auto& serie : list)
				{
					const auto unicodeSerie = StringUtils::Utf8FromString(serie);
					const auto film = inet.GetTopDir(unicodeSerie);
					const auto serieName = inet.GetTopName(film);

					std::filesystem::path serieDirecoty = workDir / std::to_wstring(i++);
					if (!std::filesystem::create_directory(serieDirecoty))
					{
						throw std::exception("Unable to create directory for serie");
					}

					const auto working_serie_directory = serieDirecoty.wstring();
					const auto seriePath = StringUtils::StringFromUtf8(film);
					FilmSource filmData{ seriePath , serie };
					const auto [files, count] = inet.CreateDownloadPartsFromFilmSource(filmData);
					inet.DownloadFilesToDirectory(files, working_serie_directory, count);
					inet.BuildMovieFromChunks(working_serie_directory, L"tsMuxeR.exe", serieName);
				}
			}
			break;
		default:
			throw std::exception("Unknown type");
		}


	}

	return 0;
}
catch (const std::exception & error)
{
	std::cout << error.what() << std::endl;
}
