cd vcpkg
cmd.exe /c "bootstrap-vcpkg.bat -disableMetrics"
.\vcpkg.exe install gtest:x64-windows boost-locale[icu]:x64-windows qt5-base:x64-windows
cd ..\build
cmake .. "-DCMAKE_TOOLCHAIN_FILE=$(Get-Location)\..\vcpkg\scripts\buildsystems\vcpkg.cmake" -G "Visual Studio 16 2019"
cmake --build . --target ALL_BUILD --config RelWithDebInfo
cd ..