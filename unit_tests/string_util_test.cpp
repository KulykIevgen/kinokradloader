#include "string_utils.h"

#include <gtest/gtest.h>
#include <string>

TEST(StringUtils, StringToWstring)
{
	const std::wstring correctValue = L"test";
	const auto testValue = StringUtils::Utf8FromString(std::string("test"));
	EXPECT_EQ(correctValue, testValue);
	EXPECT_THROW(StringUtils::Utf8FromString(std::string("����")), std::exception);
}

TEST(StringUtils, WstringToString)
{
	const std::string correctValue = "test";
	const auto testValue = StringUtils::StringFromUtf8(std::wstring(L"test"));
	EXPECT_EQ(correctValue, testValue);
}

TEST(StringUtils, ConvertStringWithLackOfData)
{
	std::string data(100, char(0));
	const char info[] = "data";
	memcpy(data.data(), info, sizeof(info));
	const auto temp = StringUtils::Utf8FromString(data);
	const auto result = StringUtils::StringFromUtf8(temp);
	EXPECT_EQ(result, std::string(info));
}